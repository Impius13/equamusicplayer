package com.example.equamusicplayer

import android.app.AlertDialog
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.*
import android.media.MediaSession2Service
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.CalendarContract.Attendees.query
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.equamusicplayer.MusicPlayer.songList
import com.example.equamusicplayer.services.BroadCastReceiverService
import com.example.equamusicplayer.services.OnClearFromRecentService
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import permissions.dispatcher.*
import java.io.File
import kotlin.math.log

/**
 * Main activity
 *
 * @constructor Create empty Main activity
 */
class MainActivity : AppCompatActivity() {

    private val broadcastReceiver = BroadCastReceiverService()
    private lateinit var notificationManager : NotificationManager

    private lateinit var tabLayout : TabLayout

    /**
     * Inflate main activity and create notification channel
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        createNotificationChannel()

        // If music is already playing or is paused, create the notification
        if (MusicPlayer.player.isPlaying) {
            PlayerNotification().createNotification(this, getString(R.string.music_channel_id), false)
        }
        else if (MusicPlayer.currentSong.title != "") {
            PlayerNotification().createNotification(this, getString(R.string.music_channel_id), true)
        }

        // Setup bottom navigation bar with navigation controller
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.fragmentContainerView)
            as NavHostFragment
        val navController = navHostFragment.navController
        val bottomNavigation = findViewById<BottomNavigationView>(R.id.bottom_bar)
        NavigationUI.setupWithNavController(bottomNavigation, navController)
    }

    /**
     * Create notification channel for playing music on minimized
     * Register broadcast receiver
     */
    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val id = getString(R.string.music_channel_id)
            val name = getString(R.string.music_channel_name)
            val descText = getString(R.string.music_channel_description)
            val importance = NotificationManager.IMPORTANCE_LOW

            val channel = NotificationChannel(id, name, importance).apply {
                description = descText
            }
            channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC

            // Register the channel in the system
            notificationManager.createNotificationChannel(channel)

            // Register broadcast receiver
            registerReceiver(broadcastReceiver, IntentFilter("PLAYING_MUSIC"))
            startService(Intent(baseContext, OnClearFromRecentService().javaClass))
        }
    }

    /**
     * Cancel all notifications and unregister receiver
     */
    override fun onDestroy() {
        super.onDestroy()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.cancelAll()
        }

        unregisterReceiver(broadcastReceiver)
    }
}