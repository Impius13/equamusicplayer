package com.example.equamusicplayer

import android.app.AlertDialog
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import permissions.dispatcher.*
import java.io.File

/**
 * Music list fragment class
 */
@RuntimePermissions
class MusicListFragment : Fragment() {

    private val songList = mutableListOf<SongData>()
    private lateinit var songRecyclerView : RecyclerView
    private lateinit var noSongsText : TextView

    private lateinit var miniSongTitle : TextView
    private lateinit var miniPausePlayButton : ImageView
    private lateinit var miniNextButton : ImageView
    private lateinit var miniPrevButton : ImageView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_music_list, container, false)

        songRecyclerView = view.findViewById(R.id.song_recycler_view)
        noSongsText = view.findViewById(R.id.no_songs_text)
        miniSongTitle = view.findViewById(R.id.mini_song_title)
        miniPausePlayButton = view.findViewById(R.id.mini_play_pause_button)
        miniNextButton = view.findViewById(R.id.mini_next_button)
        miniPrevButton = view.findViewById(R.id.mini_prev_button)

        runtimePermission()
        setResources()

        // Update song title and play\pause button on ui thread
        activity?.runOnUiThread(object : Runnable {
            override fun run() {
                if (miniSongTitle.text != MusicPlayer.currentSong.title) {
                    miniSongTitle.text = MusicPlayer.currentSong.title
                    // Notify the recycler adapter to highlight new selected song
                    songRecyclerView.adapter?.notifyItemChanged(MusicPlayer.currentIndex - 1)
                    songRecyclerView.adapter?.notifyItemChanged(MusicPlayer.currentIndex)
                    songRecyclerView.adapter?.notifyItemChanged(MusicPlayer.currentIndex + 1)
                }

                if (MusicPlayer.player.isPlaying) {
                    miniPausePlayButton.setImageResource(R.drawable.ic_pause_button)
                }
                else {
                    miniPausePlayButton.setImageResource(R.drawable.ic_play_button)
                }

                val handler = Handler()
                handler.postDelayed(this, 100)
            }
        })

        // Inflate the layout for this fragment
        return view
    }

    /**
     * Fetching the songs needs runtime permission for reading the external storage
     */
    @NeedsPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    fun runtimePermission() {
        fetchSongs()
    }

    /**
     * Show the user why is this permission needed and ask them for permission
     */
    @OnShowRationale(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    fun showRationaleForReadingStorage(request: PermissionRequest) {
        AlertDialog.Builder(activity)
            .setTitle("Permission is needed")
            .setMessage("This app needs permission for reading external storage to be able to " +
                    "fetch and play music files in memory")
            .setPositiveButton("OK") {_, _ ->
                request.proceed()
            }
            .setNegativeButton("Cancel") {_, _ ->
                request.cancel()
            }
            .show()
    }

    /**
     * If user denied the permission
     */
    @OnPermissionDenied(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    fun onReadingStorageDenied() {
        Toast.makeText(activity, "Permission denied", Toast.LENGTH_SHORT).show()
    }

    /**
     * If user wants to never ask again for this permission
     */
    @OnNeverAskAgain(android.Manifest.permission.READ_EXTERNAL_STORAGE)
    fun onReadingStorageNeverAskAgain() {
        Toast.makeText(activity, "Never asking again for permission", Toast.LENGTH_SHORT).show()
    }

    /**
     * Find all audio files on the device using MediaStore
     * Add all data to mutable list using query
     */
    fun fetchSongs() {
        val audioCollection = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        val projection = arrayOf(
            MediaStore.Audio.Media.TITLE,
            MediaStore.Audio.Media.DATA,
            MediaStore.Audio.Media.DURATION)
        val selection = "${MediaStore.Audio.Media.IS_MUSIC} != 0"
        val sortOrder = "${MediaStore.Audio.Media.DISPLAY_NAME} ASC"

        val query = activity?.applicationContext?.contentResolver?.query(
            audioCollection,
            projection,
            selection,
            null,
            sortOrder
        )
        query?.use { cursor ->
            // Get column indexes
            val titleColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE)
            val dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA)
            val durationColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION)

            while (cursor.moveToNext()) {
                val title = cursor.getString(titleColumn)
                val dataPath = cursor.getString(dataColumn)
                val duration = cursor.getString(durationColumn)

                // If the song is not deleted
                if (File(dataPath).exists())
                    songList.add(SongData(title, dataPath, duration))
            }
        }

        // If songs were found, list them using recycler adapter
        if (songList.size == 0) {
            noSongsText.visibility = View.VISIBLE
        } else {
            MusicPlayer.loadSongs(songList)
            songRecyclerView.adapter = activity?.let { SongRecyclerAdapter(it, songList) }
        }
    }

    /**
     * Set current song title and button listeners
     */
    private fun setResources() {
        miniSongTitle.text = MusicPlayer.currentSong.title

        miniPausePlayButton.setOnClickListener { pausePlay() }
        miniNextButton.setOnClickListener { playNextSong() }
        miniPrevButton.setOnClickListener { playPrevSong() }
    }

    /**
     * Pause or resume current song
     */
    private fun pausePlay() {
        MusicPlayer.pauseResume()
    }

    /**
     * Switch to next song in list and set resources
     */
    private fun playNextSong() {
        if (MusicPlayer.playNext()) {
            setResources()
            // Notify the recycler adapter to highlight new selected song
            songRecyclerView.adapter?.notifyItemChanged(MusicPlayer.currentIndex - 1)
            songRecyclerView.adapter?.notifyItemChanged(MusicPlayer.currentIndex)
        }
    }

    /**
     * Switch to previous song in list and set resources
     */
    private fun playPrevSong() {
        if (MusicPlayer.playPrev()) {
            setResources()
            // Notify the recycler adapter to highlight new selected song
            songRecyclerView.adapter?.notifyItemChanged(MusicPlayer.currentIndex + 1)
            songRecyclerView.adapter?.notifyItemChanged(MusicPlayer.currentIndex)
        }
    }

    override fun onResume() {
        super.onResume()
        songRecyclerView.adapter = activity?.let { SongRecyclerAdapter(it, songList) }
    }
}