package com.example.equamusicplayer

import android.media.MediaPlayer
import java.io.IOException

/**
 * Singleton class for media player
 */
object MusicPlayer {
    private var _currentIndex = -1
    val currentIndex: Int
        get() = _currentIndex

    private val _player: MediaPlayer = MediaPlayer()
    val player: MediaPlayer
        get() = _player

    private var _songList: MutableList<SongData> = mutableListOf()
    val songList: MutableList<SongData>
        get() = _songList

    private var _currentSong: SongData = SongData("", "", "")
    val currentSong: SongData
        get() = _currentSong

    /**
     * Set the song list
     */
    public fun loadSongs(list : MutableList<SongData>) {
        _songList = list

        // Set on song completion listener to play next song in list
        _player.setOnCompletionListener(object : MediaPlayer.OnCompletionListener {
            override fun onCompletion(player: MediaPlayer?) {
                playNext()
            }
        })
    }

    /**
     * If new song is selected, reset music player and play the song
     */
    public fun playSong(songIndex : Int) {
        if (_currentIndex != songIndex) {
            _currentIndex = songIndex
            _currentSong = _songList.get(_currentIndex)
            player.reset()
            try {
                player.setDataSource(_currentSong.path)
                player.prepare()
                player.start()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    /**
     * Pause or resume song
     */
    public fun pauseResume() {
        if (player.isPlaying) {
            player.pause()
        }
        else {
            player.start()
        }
    }

    /**
     * Switch to next song in the list
     */
    public fun playNext() : Boolean {
        if (_currentIndex == songList.size - 1) {
            return false
        }

        playSong(_currentIndex + 1)
        return true
    }

    /**
     * Switch to previous song in the list
     */
    public fun playPrev() : Boolean {
        if (_currentIndex == 0) {
            return false
        }

        playSong(_currentIndex - 1)
        return true
    }
}