package com.example.equamusicplayer

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import androidx.activity.viewModels
import androidx.fragment.app.viewModels
import java.util.concurrent.TimeUnit

/**
 * Player fragment
 *
 * @constructor Create empty Player fragment
 */
class PlayerFragment : Fragment() {

    private lateinit var titleText : TextView
    private lateinit var currentTimeText : TextView
    private lateinit var endTimeText : TextView

    private lateinit var timeBar : SeekBar

    private lateinit var pausePlayButton : ImageView
    private lateinit var nextButton : ImageView
    private lateinit var prevButton: ImageView

    /**
     * Get UI elements and setup song seekbar
     * Check for user seekbar changes in UI thread
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_player, container, false)

        titleText = view.findViewById(R.id.song_title)
        currentTimeText = view.findViewById(R.id.current_time)
        endTimeText = view.findViewById(R.id.end_time)
        timeBar = view.findViewById(R.id.length_bar)
        pausePlayButton = view.findViewById(R.id.play_pause_button)
        nextButton = view.findViewById(R.id.next_button)
        prevButton = view.findViewById(R.id.prev_button)

        setResources()

        // Update timestamp and play\pause button on ui thread
        activity?.runOnUiThread(object : Runnable {
            override fun run() {
                timeBar.setProgress(MusicPlayer.player.currentPosition)
                currentTimeText.setText(convertDuration(MusicPlayer.player.currentPosition.toString()))

                if (titleText.text != MusicPlayer.currentSong.title) {
                    setResources()
                }

                if (MusicPlayer.player.isPlaying) {
                    pausePlayButton.setImageResource(R.drawable.ic_pause_button)
                }
                else {
                    pausePlayButton.setImageResource(R.drawable.ic_play_button)
                }

                val handler = Handler()
                handler.postDelayed(this, 100)
            }
        })

        // Update timestamp in music player if user changed the seekbar
        timeBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    MusicPlayer.player.seekTo(progress)
                }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
            }
        })

        // Inflate the layout for this fragment
        return view
    }

    /**
     * Set UI elements for current song and play it
     */
    private fun setResources() {
        titleText.setText(MusicPlayer.currentSong.title)
        endTimeText.setText(convertDuration(MusicPlayer.currentSong.duration))

        timeBar.setProgress(0)
        timeBar.max = MusicPlayer.player.duration

        pausePlayButton.setOnClickListener { pausePlay() }
        nextButton.setOnClickListener { playNextSong() }
        prevButton.setOnClickListener { playPrevSong() }

        // Create notification
        activity?.let { PlayerNotification().createNotification(it.applicationContext, getString(R.string.music_channel_id), false) }
    }

    /**
     * Pause or resume current song
     */
    private fun pausePlay() {
        MusicPlayer.pauseResume()
    }

    /**
     * Switch to next song in list and set resources
     */
    private fun playNextSong() {
        if (MusicPlayer.playNext())
            setResources()
    }

    /**
     * Switch to previous song in list and set resources
     */
    private fun playPrevSong() {
        if (MusicPlayer.playPrev())
            setResources()
    }

    /**
     * Convert the song duration for timestamp
     */
    private fun convertDuration(duration : String) : String {
        val miliseconds = duration.toLong()
        return String.format("%02d:%02d",
            TimeUnit.MILLISECONDS.toMinutes(miliseconds) % TimeUnit.HOURS.toMinutes(1),
            TimeUnit.MILLISECONDS.toSeconds(miliseconds) % TimeUnit.MINUTES.toSeconds(1))
    }
}