package com.example.equamusicplayer

import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.MediaSession2Service
import android.os.Build
import android.support.v4.media.session.MediaSessionCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.equamusicplayer.services.NotificationActionService

/**
 * Player notification
 *
 * @constructor Create empty Player notification
 */
class PlayerNotification {

    /**
     * Create notification
     *
     * @param context
     * @param channelId
     * @param paused
     */
    fun createNotification(context: Context, channelId: String, paused: Boolean) {
        // Create intents for media control buttons
        val intentPausePlay = Intent(context, NotificationActionService().javaClass)
            .setAction(context.getString(R.string.notification_play))
        val pendingIntentPausePlay = PendingIntent.getBroadcast(context, 0,
            intentPausePlay, PendingIntent.FLAG_UPDATE_CURRENT)

        val intentNext = Intent(context, NotificationActionService().javaClass)
            .setAction(context.getString(R.string.notification_next))
        val pendingIntentNext = PendingIntent.getBroadcast(context, 0,
            intentNext, PendingIntent.FLAG_UPDATE_CURRENT)

        val intentPrev = Intent(context, NotificationActionService().javaClass)
            .setAction(context.getString(R.string.notification_prev))
        val pendingIntentPrev = PendingIntent.getBroadcast(context, 0,
            intentPrev, PendingIntent.FLAG_UPDATE_CURRENT)

        // Set the pause \ play button
        var pausePlayButton = R.drawable.ic_small_pause_button
        if (paused)
            pausePlayButton = R.drawable.ic_small_play_button

        // Create the notification
        var notification = NotificationCompat.Builder(context, channelId)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setContentTitle(MusicPlayer.currentSong.title)
            .setContentText("Playing song")
            .setSmallIcon(R.drawable.ic_music)
            .setOnlyAlertOnce(true)
            // Media control buttons
            .addAction(R.drawable.ic_small_prev_button, "Prev", pendingIntentPrev)
            .addAction(pausePlayButton, "Pause", pendingIntentPausePlay)
            .addAction(R.drawable.ic_small_next_button, "Next", pendingIntentNext)
            // Use media style template
            .setStyle(androidx.media.app.NotificationCompat.MediaStyle()
                .setShowActionsInCompactView(0, 1, 2)
                .setMediaSession(MediaSessionCompat(context, "tag").sessionToken))
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .build()

        val manager = NotificationManagerCompat.from(context)
        manager.notify(1, notification)
    }
}