package com.example.equamusicplayer

import java.io.Serializable

/**
 * Song data
 *
 * @property title
 * @property path
 * @property duration
 * @constructor Create empty Song data
 */
class SongData(var title : String, var path : String, var duration : String) : Serializable {
}