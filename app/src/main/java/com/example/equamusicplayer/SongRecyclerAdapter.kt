package com.example.equamusicplayer

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.MediaPlayer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.NonDisposableHandle.parent

/**
 * Song recycler adapter
 *
 * @property context
 * @property songList
 * @constructor Create empty Song recycler adapter
 */
class SongRecyclerAdapter(var context : Context, var songList : MutableList<SongData>) : RecyclerView.Adapter<SongRecyclerAdapter.ViewHolder>() {

    /**
     * Inflate layout
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongRecyclerAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(context)
        val view = layoutInflater.inflate(R.layout.recycler_song_item, parent, false)

        return SongRecyclerAdapter.ViewHolder(view)
    }

    /**
     * Set title text for recycler item and check for user selection
     */
    override fun onBindViewHolder(holder: SongRecyclerAdapter.ViewHolder, position: Int) {
        val songData = songList.get(position)
        holder.title.text = songData.title

        // Differentiate the current played song
        if (MusicPlayer.currentIndex == position) {
            holder.title.setTextColor(Color.parseColor("#8e24aa"))
        }
        else {
            holder.title.setTextColor(Color.parseColor("#eeeeee"))
        }

        holder.itemView.setOnClickListener {
            // If user selected this item, change index in song list and
            // switch to player fragment
            MusicPlayer.playSong(position)

            Navigation.findNavController(holder.itemView).navigate(R.id.navigate_to_player_fragment)
        }
    }

    /**
     * Return size of the song list
     */
    override fun getItemCount(): Int {
        return songList.size
    }

    /**
     * View holder
     *
     * @constructor
     *
     * @param itemView
     */
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.recycler_song_name)
        val icon: ImageView = itemView.findViewById(R.id.img_song)
    }
}