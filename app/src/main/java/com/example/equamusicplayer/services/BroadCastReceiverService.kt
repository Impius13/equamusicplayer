package com.example.equamusicplayer.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.example.equamusicplayer.MusicPlayer
import com.example.equamusicplayer.PlayerNotification
import com.example.equamusicplayer.R

/**
 * Broad cast receiver service
 *
 * @constructor Create empty Broad cast receiver service
 */
class BroadCastReceiverService : BroadcastReceiver() {
    /**
     * Receive pressed button and do action accordingly
     */
    override fun onReceive(context: Context, intent: Intent) {
        val action = intent?.extras?.getString("action_name")

        when(action) {
            context?.getString(R.string.notification_play) ->
            {
                MusicPlayer.pauseResume()
                val playing = MusicPlayer.player.isPlaying
                PlayerNotification().createNotification(context, context.getString(R.string.music_channel_id), !playing)
            }
            context?.getString(R.string.notification_next) ->
            {
                MusicPlayer.playNext()
                PlayerNotification().createNotification(context, context.getString(R.string.music_channel_id), false)
            }
            context?.getString(R.string.notification_prev) ->
            {
                MusicPlayer.playPrev()
                PlayerNotification().createNotification(context, context.getString(R.string.music_channel_id), false)
            }
        }
    }
}