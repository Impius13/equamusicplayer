package com.example.equamusicplayer.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

/**
 * Notification action service
 *
 * @constructor Create empty Notification action service
 */
class NotificationActionService: BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        context?.sendBroadcast(Intent("PLAYING_MUSIC")
            .putExtra("action_name", intent?.action))
    }
}